import { User } from '../users/user.entity';

/**
 * Сущность поста делайте сами, опирайтесь на пример User Entity
 */
export class Post {
  /**
   * Не забудьте, что у поста есть автор!
   * Вам нужно связать две сущности
   */
  author?: User;

  authorId?: string;
}
